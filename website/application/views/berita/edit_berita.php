<div class="container-fluid">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Berita</h1>
          </div>
          <p class="mb-4">Anda dapat mengubah data berita disini.</p>

          <div class="row">

            <div class="col-lg-6">
              <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Edit Berita</h6>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <a class="dropdown-item" href="<?= site_url('berita');?>">Exit</a>
                    </div>
                  </div>
                </div>
                <div class="card-body">
                  <form class="user" action="<?= site_url('berita/update');?>" method="POST" data-parsley-validate="true">
                    

                    <div class="form-group">
                      <label>Kode Berita</label>
                      <input type="text" class="form-control" name="id_berita" data-parsley-type="alphanum" data-parsley-required="true" value="<?= $kd_berita;?>" readonly>
                    </div>

                    <div class="form-group">
                      <label>Tanggal Berita</label>
                      <input type="date" class="form-control" name="tanggal"  data-parsley-required="true" value="<?= $tanggal;?>">
                    </div>

                    <div class="form-group">
                      <label>Keterangan</label>
                      <input type="text" class="form-control" name="keterangan" data-parsley-type="alphanum" data-parsley-required="true" value="<?= $keterangan;?>">
                    </div>

                    <div class="form-group">
                      <label>Foto</label>
                      <input type="file" class="form-control" name="foto" data-parsley-type="number" data-parsley-required="true" value="<?= $foto;?>">
                    </div>

                    <div class="form-group">
                      <label>Judul</label>
                      <input type="text" class="form-control" name="judul"  data-parsley-required="true" value="<?= $judul;?>">
                    </div>
                    
                    <button class="btn btn-primary" type="submit" >Update</button>
                  </form>
                </div>
              </div>

            </div>

          </div>

        </div>