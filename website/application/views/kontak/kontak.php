<div class="container-fluid">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Kontak</h1>
            <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" data-toggle="modal" data-target="#bahanModal">
              <i class="fas fa-plus"></i> Tambah</a>
          </div>
          <p class="mb-4">Anda dapat menambahkan kontak disini.</p>
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Daftar Kontak</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Alamat</th>
                      <th>No Telepon</th>
                      <th>Email</th>
                      <th>Instagram</th>
                      <th>Twitter</th>
                      <th>Facebook</th>
                      <th>Youtube</th>
                      <th style="text-align: center;">Action</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                        <th>No</th>>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th>No Telepon</th>
                        <th>Email</th>
                        <th>Instagram</th>
                        <th>Twitter</th>
                        <th>Facebook</th>
                        <th>Youtube</th>
                        <th style="text-align: center;">Action</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    <?php $no=1;
                      foreach ($kontak as $data){ ?>
                          <tr>
                            <td><?= $no;?></td>
                            <td><?= $data->nama;?></td>
                            <td><?= $data->alamat;?></td>
                            <td><?= $data->no_telp;?></td>
                            <td><?= $data->email;?></td>
                            <td><?= $data->ig;?></td>
                            <td><?= $data->twt;?></td>
                            <td><?= $data->fb;?></td>
                            <td><?= $data->yt;?></td>
                            <td style="text-align: center;">
                                <a class="btn btn-primary" href="kontak/update/<?= $data->id;?>"><i class="fa fa-edit"></i></a>
                                <a href="kontak/delete/<?= $data->id;?>" class="btn btn-danger" onclick="return confirm('Data yang dihapus tidak dapat ditampilkan kembali!');" ><i class="far fa-trash-alt"></i></a>
                            </td>
                          </tr>
                      <?php $no++; }?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

  <div class="modal fade" id="bahanModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Kontak</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">

                  <form class="user" action="<?= site_url('kontak/tambah');?>" method="POST" data-parsley-validate="true" enctype="multipart/form-data">
                    
                    <div class="form-group">
                      <label>Kode Kontak</label>
                      <input type="text" class="form-control" name="id" data-parsley-type="alphanum" data-parsley-required="true" value="<?= $no;?>" readonly>
                    </div>

                    <div class="form-group">
                      <label>Nama</label>
                      <input type="text" class="form-control" placeholder="Masukan disini." name="nama"  data-parsley-required="true">
                    </div>

                    <div class="form-group">
                      <label>Alamat</label>
                      <input type="text" class="form-control" placeholder="Masukan disini." name="alamat"  data-parsley-required="true">
                    </div>

                    <div class="form-group">
                      <label>No Telepon</label>
                      <input type="text" class="form-control" placeholder="Masukan disini." name="no_telp"  data-parsley-required="true">
                    </div>

                    <div class="form-group">
                      <label>Email</label>
                      <input type="text" class="form-control" placeholder="Masukan disini." name="email" data-parsley-type="email" data-parsley-required="true">
                    </div>

                    <div class="form-group">
                      <label>Instagram</label>
                      <input type="text" class="form-control" placeholder="Masukan disini." name="ig" data-parsley-type="alphanum" data-parsley-required="true">
                    </div>

                    <div class="form-group">
                      <label>Twitter</label>
                      <input type="text" class="form-control" placeholder="Masukan disini." name="twt" data-parsley-type="alphanum" data-parsley-required="true">
                    </div>

                    <div class="form-group">
                      <label>Facebook</label>
                      <input type="text" class="form-control" placeholder="Masukan disini." name="fb" data-parsley-type="alphanum" data-parsley-required="true">
                    </div>

                    <div class="form-group">
                      <label>Youtube</label>
                      <input type="text" class="form-control" placeholder="Masukan disini." name="yt" data-parsley-type="alphanum" data-parsley-required="true">
                    </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary" type="submit" >Tambah</button>
          </form>
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>