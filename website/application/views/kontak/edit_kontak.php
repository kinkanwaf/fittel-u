<div class="container-fluid">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Kontak</h1>
          </div>
          <p class="mb-4">Anda dapat mengubah data kontak disini.</p>

          <div class="row">

            <div class="col-lg-6">
              <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Edit Kontak</h6>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <a class="dropdown-item" href="<?= site_url('kontak');?>">Exit</a>
                    </div>
                  </div>
                </div>
                <div class="card-body">
                  <form class="user" action="<?= site_url('kontak/update');?>" method="POST" data-parsley-validate="true">
                    
                    <div class="form-group">
                      <label>Kode Kontak</label>
                      <input type="text" class="form-control" name="id" data-parsley-type="alphanum" data-parsley-required="true" value="<?= $id;?>" readonly>
                    </div>

                    <div class="form-group">
                      <label>Nama</label>
                      <input type="text" class="form-control" placeholder="Masukan disini." name="nama"  data-parsley-required="true" value="<?= $nama;?>">
                    </div>

                    <div class="form-group">
                      <label>Alamat</label>
                      <input type="text" class="form-control" placeholder="Masukan disini." name="alamat"  data-parsley-required="true" value="<?= $alamat;?>">
                    </div>

                    <div class="form-group">
                      <label>No Telepon</label>
                      <input type="text" class="form-control" placeholder="Masukan disini." name="no_telp"  data-parsley-required="true" value="<?= $no_telp;?>">
                    </div>

                    <div class="form-group">
                      <label>Email</label>
                      <input type="text" class="form-control" placeholder="Masukan disini." name="email" data-parsley-type="email" data-parsley-required="true" value="<?= $email;?>">
                    </div>

                    <div class="form-group">
                      <label>Instagram</label>
                      <input type="text" class="form-control" placeholder="Masukan disini." name="ig" data-parsley-type="alphanum" data-parsley-required="true" value="<?= $ig;?>">
                    </div>

                    <div class="form-group">
                      <label>Twitter</label>
                      <input type="text" class="form-control" placeholder="Masukan disini." name="twt" data-parsley-type="alphanum" data-parsley-required="true" value="<?= $twt;?>">
                    </div>

                    <div class="form-group">
                      <label>Facebook</label>
                      <input type="text" class="form-control" placeholder="Masukan disini." name="fb" data-parsley-type="alphanum" data-parsley-required="true" value="<?= $fb;?>">
                    </div>

                    <div class="form-group">
                      <label>Youtube</label>
                      <input type="text" class="form-control" placeholder="Masukan disini." name="yt" data-parsley-type="alphanum" data-parsley-required="true" value="<?= $yt;?>">
                    </div>
                    
                    <button class="btn btn-primary" type="submit" >Update</button>
                  </form>
                </div>
              </div>

            </div>

          </div>

        </div>