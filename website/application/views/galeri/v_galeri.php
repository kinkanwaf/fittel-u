<div class="container-fluid">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">galeri</h1>
            <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" data-toggle="modal" data-target="#bahanModal">
              <i class="fas fa-plus"></i> Tambah</a>
          </div>
          <p class="mb-4">Anda dapat menambahkan galeri disini.</p>
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Daftar galeri</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Tanggal galeri</th>
                      <th>Kode galeri</th>
                      <th>Keterangan</th>
                      <th>Foto</th>
                      <th>Judul</th>
                      <th style="text-align: center;">Action</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Tanggal galeri</th>
                        <th>Kode galeri</th>
                        <th>Keterangan</th>
                        <th>Foto</th>
                        <th>Judul</th>
                        <th style="text-align: center;">Action</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    <?php $no=1;
                      foreach ($galeri as $data){ ?>
                          <tr>
                            <td><?= $no;?></td>
                            <td><?= $data->tanggal;?></td>
                            <td><?= $data->id;?></td>
                            <td><?= $data->keterangan;?></td>
                            <td><?= $data->foto;?></td>
                            <td><?= $data->judul;?></td>
                            <td style="text-align: center;">
                                <a class="btn btn-primary" href="galeri/update/<?= $data->id;?>"><i class="fa fa-edit"></i></a>
                                <a href="galeri/delete/<?= $data->id;?>" class="btn btn-danger" onclick="return confirm('Data yang dihapus tidak dapat ditampilkan kembali!');" ><i class="far fa-trash-alt"></i></a>
                            </td>
                          </tr>
                      <?php $no++; }?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

  <div class="modal fade" id="bahanModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah galeri</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">

                  <form class="user" action="<?= site_url('galeri/tambah');?>" method="POST" data-parsley-validate="true">
                    
                    <div class="form-group">
                      <label>Kode galeri</label>
                      <input type="text" class="form-control" name="id" data-parsley-type="alphanum" data-parsley-required="true" value="<?= $kd_galeri;?>" readonly>
                    </div>

                    <div class="form-group">
                      <label>Tanggal galeri</label>
                      <input type="date" class="form-control" placeholder="Masukan disini." name="tanggal"  data-parsley-required="true">
                    </div>

                    <div class="form-group">
                      <label>Keterangan</label>
                      <input type="text" class="form-control" placeholder="Masukan disini." name="keterangan" data-parsley-type="alphanum" data-parsley-required="true">
                    </div>

                    <div class="form-group">
                      <label>Foto</label>
                      <input type="text" class="form-control" placeholder="Masukan disini." name="foto" data-parsley-type="number" data-parsley-required="true">
                    </div>

                    <div class="form-group">
                      <label>Judul</label>
                      <input type="text" class="form-control" placeholder="Masukan disini." name="judul"  data-parsley-required="true">
                    </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary" type="submit" >Tambah</button>
          </form>
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>