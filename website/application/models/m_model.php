<?php
class m_model extends CI_Model
{
    public function berita()
    {
        $this->db->select('*');
        $this->db->from('berita');
        $query = $this->db->get();
        return $query->result();
    }
    public function kd_berita()
    {
        $this->db->select('RIGHT(berita.id_berita,3) as kode', FALSE);
        $this->db->order_by('id_berita','DESC');
        $this->db->limit(1);
        $query = $this->db->get('berita');

        if($query->num_rows()!=0)
        {
            $data = $query->row();
            $kode = intval($data->kode)+1;
        }
        else
        {
            $kode=1;
        }
        $kode_max = str_pad($kode,3,"0",STR_PAD_LEFT);
        $fix_code = "B".$kode_max;
        return $fix_code;
    }
    public function update_berita()
    {
        $this->db->select('*');
        $this->db->from('berita');
        $this->db->where('id_berita');
        $query = $this->db->get();
        return $query->row_array();
    }
    public function check_foto()
    {
        $config['upload_path']  = './assets/foto';
        $config['allowed_types']= 'gif|jpg|pdf|png';
        $config['max_size']     = 4000;
        $config['max_width']    = 4160;
        $config['max_height']   = 3120;
        $config['encrypt_name'] = TRUE;
        $config['remove_spaces']= TRUE;
        $config['detect_mime']  = TRUE;
        $config['mod_mime_fix'] = TRUE;

        $this->load->library('upload', $config);

        if($this->upload->do_upload('foto'))
        {
            $return = ['reesult' => 'success', 'foto' => $this->upload->data()];
            return $return;
        }
        else
        {
            $return = ['result' => 'failed'];
            return $return;
        }
    }
    public function save_foto($upload)
    {
        $data = ['foto' => $upload['foto']];
        $this->db->where('md5(id_berita)', $this->input->post('id'));
        return $this->db->update('berita', $data);
    }
    public function kontak()
    {
        $this->db->select('*');
        $this->db->from('kontak');
        $query = $this->db->get();
        return $query->result();
    }
    public function update_kontak()
    {
        $this->db->select('*');
        $this->db->from('kontak');
        $this->db->where('id');
        $query = $this->db->get();
        return $query->row();
    }
}