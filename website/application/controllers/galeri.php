<?php
class galeri extends CI_Controller
{
    public function index()
    {
        $data = [
                    'galeri' => $this->m_model->galeri(),
                    'kd_galeri' => $this->m_model->kd_galeri()
        ];
        $this->template->load('template','v_galeri',$data);
    }
    public function tambah()
    {
        $data = [
                'id' => $this->input->post('id'),
                'nama'   => $this->input->post('nama'),
                'deskripsi'=> $this->input->post('deskripsi'),
                'nama_file' => $this->input->post('nama_file'),
                'ukuran' => $this->input->post('ukuran'),
                'tipe'  => $this->input->post('tipe')
        ];
        $this->db->insert('galeri',$data);
    }
    public function update()
    {
        $update = $this->uri->segment(3);

        $this->db->select('id');
        $this->db->from('galeri');
        $this->db->where('id',$update);
        $query = $this->db->get()->row_array();

        if($query['id'])
        {
            $get    = $this->m_model->update_galeri();
            $data   = [
                        'nama'   => $get['nama'],
                        'deskripsi'=> $get['deskripsi'],
                        'nama_file'      => $get['nama_file'],
                        'ukuran'     => $get['ukuran'],
                        'tipe' => $get['tipe']
            ];
            $this->template->load('template','edit_galeri');
        }
        else
        {
            redirect('galeri');
        }
    }
    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('galeri');
        redirect('galeri');
    }
}