<?php
class kontak extends CI_Controller
{
    public function index()
    {
        $data = [
                    'kontak' => $this->m_model->kontak()
        ];
        $this->template->load('template','kontak/kontak',$data);
    }
    public function tambah()
    {
        $data = [
                'id' => $this->input->post('id'),
                'nama' => $this->input->post('nama'),
                'alamat' => $this->input->post('alamat'),
                'no_telp'=> $this->input->post('no_telp'),
                'email'  => $this->input->post('email'),
                'ig' => $this->input->post('ig'),
                'twt' => $this->input->post('twt'),
                'fb' => $this->input->post('fb'),
                'yt' => $this->input->post('yt')
        ];
        $this->db->insert('kontak',$data);
        redirect('kontak');
    }
    public function update()
    {
        $update = $this->uri->segment(3);

        $this->db->select('id');
        $this->db->from('kontak');
        $this->db->where('id',$update);
        $query = $this->db->get()->row_array();

        if($query['id'])
        {
            $get    = $this->m_model->update_kontak();
            $data   = [
                        'id' => $get['id'],
                        'nama' => $get['nama'],
                        'alamat' => $get['alamat'],
                        'no_telp'=> $get['no_telp'],
                        'email' => $get['email'],
                        'ig' => $get['ig'],
                        'twt'=> $get['twt'],
                        'fb' => $get['fb'],
                        'yt' => $get['yt']
            ];
            $this->template->load('template','kontak/edit_kontak');
        }
        else
        {
            redirect('kontak');
        }
    }
    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('kontak');
        redirect('kontak');
    }
}