<?php
class berita extends CI_Controller
{
    public function index()
    {
        $data = [
                    'berita' => $this->m_model->berita(),
                    'kd_berita' => $this->m_model->kd_berita()
        ];
        $this->template->load('template','berita/v_berita',$data);
    }
    public function tambah()
    {
        $data = [
                'id_berita' => $this->input->post('id_berita'),
                'tanggal'   => date('Y-m-d'),
                'keterangan'=> $this->input->post('keterangan'),
                'foto'      => $this->input->post('foto'),
                'judul'     => $this->input->post('judul')
        ];
        $this->db->insert('berita',$data);
        redirect('berita');
    }
    public function update_foto()
    {
        if($this->input->post('foto'))
        {
            $upload = $this->m_model->check_foto();
            if($upload['result'] = "success")
            {
                $this->m_model->save_foto($upload['file']);
                $this->session->set_flashdata('success_msg','Foto Berhasil Disimpan');
                redirect('berita');
            }
            else
            {
                $this->session->set_flashdata('error_msg','Maaf terjadi kesalahan, cek ukuran dan resolusi gambar');
                redirect('berita');
            }
        }
        else
        {
            $this->session->set_flashdata('error_msg','Maaf perintah dibatalkan');
            redirect('berita');
        }
    }
    public function update()
    {
        $update = $this->uri->segment(3);

        $this->db->select('id_berita');
        $this->db->from('berita');
        $this->db->where('id_berita',$update);
        $query = $this->db->get()->row_array();

        if($query['id_berita'])
        {
            $get    = $this->m_model->update_berita();
            $data   = [
                        'tanggal'   => $get['tanggal'],
                        'keterangan'=> $get['keterangan'],
                        'foto'      => $get['foto'],
                        'judul'     => $get['judul']
            ];
            $this->template->load('template','berita/edit_berita');
        }
        else
        {
            redirect('berita');
        }
    }
    public function delete($id)
    {
        $this->db->where('id_berita', $id);
        $this->db->delete('berita');
        redirect('berita');
    }
}