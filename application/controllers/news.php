<?php
class news extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_data');
    }

    public function index()
    {
        $data = [
            'title'             => 'Landing Page | News',
            'url'               => 'Masterdata',
            'sub_url'           => 'News',
            'update_news'       => 'FALSE',
            'update_news_img'   => 'FALSE',
            'data'              => $this->m_data->get_news()
        ];
        $this->load->view('template/header', $data);
        $this->load->view('template/sidebar');
        $this->load->view('content/news/index', $data);
        $this->load->view('template/footer');
    }

    public function add_news()
    {
        if ($this->input->post('title')) {
            $insert = $this->m_data->add_news();
            if ($insert) {
                $upload = $this->m_data->check_file_upload();
                if ($upload['result'] == "success") {
                    $this->m_data->save_file($upload['file']);
                    $this->session->set_flashdata('succses_msg', 'Data Saved.');
                    redirect('news');
                } else {
                    redirect('news');
                    $this->session->set_flashdata('error_msg', 'Failed upload images.');
                }
            } else {
                $this->session->set_flashdata('error_msg', 'Failed to save data.');
                redirect('news');
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Sorry, your request not found.');
            redirect('news');
        }
    }

    public function active()
    {
        if ($this->uri->segment('3')) {
            $get = $this->m_data->get_news_id($this->uri->segment('3'));
            if ($get['news_id']) {
                $this->m_data->active_news($get['news_id']);
                $this->session->set_flashdata('succses_msg', 'Data activated');
                redirect('news');
            } else {
                $this->session->set_flashdata('error_msg', 'Sorry, your request not found');
                redirect('news');
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Sorry, your request canceled');
            redirect('news');
        }
    }

    public function non_active()
    {
        if ($this->uri->segment('3')) {
            $get = $this->m_data->get_news_id($this->uri->segment('3'));
            if ($get['news_id']) {
                $this->m_data->non_active_news($get['news_id']);
                $this->session->set_flashdata('succses_msg', 'Data non activated');
                redirect('news');
            } else {
                $this->session->set_flashdata('error_msg', 'Sorry, your request not found');
                redirect('news');
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Sorry, your request canceled');
            redirect('news');
        }
    }

    public function edit_news()
    {
        if ($this->uri->segment('3')) {
            $edit_news = $this->m_data->edit_news($this->uri->segment('3'));
            if ($edit_news['news_id']) {
                $data = [
                    'title'             => 'Landing Page | Update News',
                    'url'               => 'Masterdata',
                    'sub_url'           => 'Update News',
                    'update_news'       => 'TRUE',
                    'update_news_img'   => 'FALSE',
                    'data'              => $this->m_data->get_news(),
                    'news_id'           => $edit_news['news_id'],
                    'date'              => $edit_news['date_upload'],
                    'title_'            => $edit_news['title'],
                    'news'              => $edit_news['news'],
                    'writer'            => $edit_news['writer'],
                ];
                $this->load->view('template/header', $data);
                $this->load->view('template/sidebar');
                $this->load->view('content/news/index', $data);
                $this->load->view('template/footer');
            } else {
                $this->session->set_flashdata('error_msg', 'Sorry, your request not found');
                redirect('news');
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Sorry, your request canceled');
            redirect('news');
        }
    }

    public function edit_news_img()
    {
        if ($this->uri->segment('3')) {
            $edit_news = $this->m_data->edit_news($this->uri->segment('3'));
            if ($edit_news['news_id']) {
                $data = [
                    'title'             => 'Landing Page | Update News',
                    'url'               => 'Masterdata',
                    'sub_url'           => 'Update News',
                    'update_news'       => 'FALSE',
                    'update_news_img'   => 'TRUE',
                    'data'              => $this->m_data->get_news(),
                    'news_id'           => $edit_news['news_id'],
                    'images'            => $edit_news['img_upload'],
                ];
                $this->load->view('template/header', $data);
                $this->load->view('template/sidebar');
                $this->load->view('content/news/index', $data);
                $this->load->view('template/footer');
            } else {
                $this->session->set_flashdata('error_msg', 'Sorry, your request not found');
                redirect('news');
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Sorry, your request canceled');
            redirect('news');
        }
    }

    public function update_news()
    {
        if ($this->input->post('news_id')) {
            $this->m_data->update_news();
            $this->session->set_flashdata('succses_msg', 'Data update');
            redirect('news');
        } else {
            $this->session->set_flashdata('error_msg', 'Sorry, your request canceled');
            redirect('news');
        }
    }

    public function update_news_img()
    {
        if ($this->input->post('news_id')) {
            $upload = $this->m_data->check_file_upload();
            if ($upload['result'] == "success") {
                $this->m_data->update_news_img($upload['file']);
                $this->session->set_flashdata('succses_msg', 'Data Saved.');
                redirect('news');
            } else {
                redirect('news');
                $this->session->set_flashdata('error_msg', 'Failed upload images.');
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Sorry, your request canceled');
            redirect('news');
        }
    }
}
