<?php
class videos extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_data');
    }

    public function index()
    {
        $data = [
            'title'             => 'Landing Page | Videos',
            'url'               => 'Masterdata',
            'sub_url'           => 'Videos',
            'update_videos'     => 'FALSE',
            'data'              => $this->m_data->get_videos()
        ];
        $this->load->view('template/header', $data);
        $this->load->view('template/sidebar');
        $this->load->view('content/videos/index', $data);
        $this->load->view('template/footer');
    }

    public function add_videos()
    {
        if ($this->input->post('title')) {
            $insert = $this->m_data->add_videos();
            if ($insert) {
                $this->session->set_flashdata('succses_msg', 'Data Saved.');
                redirect('videos');
            } else {
                $this->session->set_flashdata('error_msg', 'Failed to save data.');
                redirect('videos');
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Sorry, your request not found.');
            redirect('videos');
        }
    }

    public function active()
    {
        if ($this->uri->segment('3')) {
            $get = $this->m_data->get_videos_id($this->uri->segment('3'));
            if ($get['videos_id']) {
                $this->m_data->active_videos($get['videos_id']);
                $this->session->set_flashdata('succses_msg', 'Data activated');
                redirect('videos');
            } else {
                $this->session->set_flashdata('error_msg', 'Sorry, your request not found');
                redirect('videos');
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Sorry, your request canceled');
            redirect('videos');
        }
    }

    public function non_active()
    {
        if ($this->uri->segment('3')) {
            $get = $this->m_data->get_videos_id($this->uri->segment('3'));
            if ($get['videos_id']) {
                $this->m_data->non_active_videos($get['videos_id']);
                $this->session->set_flashdata('succses_msg', 'Data non activated');
                redirect('videos');
            } else {
                $this->session->set_flashdata('error_msg', 'Sorry, your request not found');
                redirect('videos');
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Sorry, your request canceled');
            redirect('videos');
        }
    }

    public function edit_videos()
    {
        if ($this->uri->segment('3')) {
            $edit_videos = $this->m_data->edit_videos($this->uri->segment('3'));
            if ($edit_videos['videos_id']) {
                $data = [
                    'title'             => 'Landing Page | Update Videos',
                    'url'               => 'Masterdata',
                    'sub_url'           => 'Update Videos',
                    'update_videos'     => 'TRUE',
                    'data'              => $this->m_data->get_videos(),
                    'videos_id'         => $edit_videos['videos_id'],
                    'date'              => $edit_videos['date'],
                    'title_'            => $edit_videos['title'],
                    'url_'              => $edit_videos['url']
                ];
                $this->load->view('template/header', $data);
                $this->load->view('content/videos/sidebar');
                $this->load->view('content/videos/index', $data);
                $this->load->view('template/footer');
            } else {
                $this->session->set_flashdata('error_msg', 'Sorry, your request not found');
                redirect('videos');
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Sorry, your request canceled');
            redirect('videos');
        }
    }

    public function update_videos()
    {
        if ($this->input->post('videos_id')) {
            $this->m_data->update_videos();
            $this->session->set_flashdata('succses_msg', 'Data update');
            redirect('videos');
        } else {
            $this->session->set_flashdata('error_msg', 'Sorry, your request canceled');
            redirect('videos');
        }
    }
}
