<?php
class gallery extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_data');
    }
    public function index()
    {
        $data = [
            'title'     => 'Landing Pages | Gallery',
            'url'       => 'Master Data',
            'sub_url'   => 'Gallery',
            'data'      => $this->m_data->get_gallery()
        ];
        $this->load->view('template/header', $data);
        $this->load->view('template/sidebar');
        $this->load->view('content/gallery/index', $data);
        $this->load->view('template/footer');
    }
    public function add_gallery()
    {
        if($this->input->title)
        {
            $insert = $this->m_data->add_gallery();
            if ($insert) {
                $upload = $this->m_data->check_file_upload_gallery();
                if ($upload['result'] == "success") {
                    $this->m_data->save_file_gallery($upload['file']);
                    $this->session->set_flashdata('succses_msg', 'Data Saved.');
                    redirect('gallery');
                } else {
                    $this->session->set_flashdata('error_msg', 'Failed upload images.');
                    redirect('gallery');
                }
            } else {
                $this->session->set_flashdata('error_msg', 'Failed to save data.');
                redirect('gallery');
            }
        }else{
            $this->session->set_flashdata('error_msg', 'Sorry, your request not found.');
            redirect('gallery');
        }
    }
    public function active()
    {
        if ($this->uri->segment('3')) {
            $get = $this->m_data->get_gallery_id($this->uri->segment('3'));
            if ($get['gallery_id']) {
                $this->m_data->active_gallery($get['gallery_id']);
                $this->session->set_flashdata('succses_msg', 'Data activated');
                redirect('gallery');
            } else {
                $this->session->set_flashdata('error_msg', 'Sorry, your request not found');
                redirect('gallery');
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Sorry, your request canceled');
            redirect('gallery');
        }
    }

    public function non_active()
    {
        if ($this->uri->segment('3')) {
            $get = $this->m_data->get_gallery_id($this->uri->segment('3'));
            if ($get['gallery_id']) {
                $this->m_data->non_active_gallery($get['gallery_id']);
                $this->session->set_flashdata('succses_msg', 'Data non activated');
                redirect('gallery');
            } else {
                $this->session->set_flashdata('error_msg', 'Sorry, your request not found');
                redirect('gallery');
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Sorry, your request canceled');
            redirect('gallery');
        }
    }
}

?>