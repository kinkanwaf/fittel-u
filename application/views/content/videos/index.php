<div id="content" class="content">
    <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><?= $url ?></a></li>
        <li class="breadcrumb-item active"><?= $sub_url ?></li>
    </ul>
    <h1 class="page-header">
        <?= $sub_url ?> <small>page header description goes here...</small>
    </h1>
    <div id="rootwizard" class="wizard wizard-full-width">
        <div class="wizard-header">
            <ul class="nav nav-pills">
                <li><a href="#tab1" data-toggle="tab">Add Videos</a></li>
                <li><a href="#tab2" data-toggle="tab" class="active">View Videos</a></li>
                <li><a href="#tab3" data-toggle="tab">Edit Videos</a></li>
            </ul>
        </div>
        <div class="wizard-content tab-content">
            <div class="tab-pane" id="tab1">
                <div class="row">
                    <div class="col-md-6 offset-md-3">
                        <p class="desc m-b-20">Make sure to use a valid email address, you'll need to verify it before you can login to your account.</p>
                        <form action="<?= site_url('videos/add_videos') ?>" method="POST" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="control-label">Date <span class="text-danger">*</span></label>
                                <input type="date" class="form-control" value="<?= date('Y-m-d') ?>" name="date" />
                            </div>
                            <div class="form-group">
                                <label class="control-label">Title <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="title" placeholder="Input here.." data-parsley-required="true" />
                            </div>
                            <div class="form-group">
                                <label class="control-label">URL Videos Youtube <span class="text-danger">*</span></label>
                                <textarea class="form-control" name="url" rows="5" placeholder="Input here.." data-parsley-required="true"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary btn-sm m-r-3 m-b-3 start">
                                <i class="glyphicon glyphicon-upload"></i>
                                <span>Save</span>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="tab-pane active" id="tab2">
                <h1 class="page-header">
                    View News <small>page header description goes here...</small>
                </h1>
                <table id="datatables-default" class="table table-striped table-condensed table-bordered bg-white">
                    <thead>
                        <tr>
                            <th class="no-sort" style="width:1%">#</th>
                            <th style="white-space: nowrap">DATE</th>
                            <th style="white-space: nowrap">TITLE</th>
                            <th style="white-space: nowrap">YOUTUBE VIEW</th>
                            <th style="white-space: nowrap">STATUS</th>
                            <th class="no-sort" style="width:1%"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;
                        foreach ($data as $row) { ?>
                            <tr>
                                <td><?= $no; ?></td>
                                <td><?= date('d F Y', strtotime($row->date)); ?></td>
                                <td><?= $row->title; ?></td>
                                <td>
                                    <iframe width="450" height="250" src="<?= $row->url; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </td>
                                <td>
                                    <?php if ($row->status == '1') { ?>
                                        <label class="badge badge-success">
                                            <a style="color: #fff;" href="<?= site_url('videos/non_active' . '/' . md5($row->videos_id)); ?>" onclick="return confirm('are you sure non active data?')">Active</a>
                                        </label>
                                    <?php } elseif ($row->status == '0') { ?>
                                        <label class="badge badge-warning">
                                            <a style="color: #fff;" href="<?= site_url('videos/active' . '/' . md5($row->videos_id)); ?>" onclick="return confirm('are you sure active data?')">Not Active</a>
                                        </label>
                                    <?php } ?>
                                </td>
                                <td class="btn-col" style="white-space: nowrap">
                                    <a href="<?= site_url('videos/edit_videos' . '/' . md5($row->videos_id)); ?>" class="btn btn-default btn-xs"><i class="ti-pencil"></i></a>
                                </td>
                            </tr>
                        <?php $no++;
                        } ?>
                    </tbody>
                </table>
            </div>
            <div class="tab-pane" id="tab3">
                <div class="row">
                    <div class="col-md-6 offset-md-3">
                        <p class="desc m-b-20">Make sure to use a valid email address, you'll need to verify it before you can login to your account.</p>
                        <?php if ($update_videos == "TRUE") { ?>
                            <form action="<?= site_url('videos/update_videos') ?>" method="POST" data-parsley-validate="true">
                                <input type="text" class="form-control" value="<?= md5($videos_id) ?>" name="videos_id" hidden />
                                <div class="form-group">
                                    <label class="control-label">Date <span class="text-danger">*</span></label>
                                    <input type="date" class="form-control" value="<?= $date; ?>" name="date" data-parsley-required="true" />
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Title <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="title" value="<?= $title_; ?>" data-parsley-required="true" />
                                </div>
                                <div class="form-group">
                                    <label class="control-label">URL Videos Youtube <span class="text-danger">*</span></label>
                                    <textarea class="form-control" name="url" rows="5" data-parsley-required="true"><?= $url_; ?></textarea>
                                </div>
                                <button type="submit" class="btn btn-primary btn-sm m-r-3 m-b-3 start">
                                    <i class="glyphicon glyphicon-upload"></i>
                                    <span>Update</span>
                                </button>
                            </form>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>