<div id="sidebar" class="sidebar sidebar-inverse">
    <div data-scrollbar="true" data-height="100%">
        <ul class="nav">
            <li class="nav-profile">
                <div class="image">
                    <img src="<?= site_url(); ?>assets/img/user.jpg" />
                </div>
                <div class="info">
                    <h4>Sean Ngu</h4>
                    <p>UXUI Frontend Developer</p>
                </div>
            </li>
            <li class="nav-divider"></li>
            <li class="nav-header">Navigation</li>
            <li><a href="#"><i class="ti-home"></i><span>Home</span></a></li>
            <li><a href="#"><i class="ti-pie-chart"></i><span>Analytics</span></a></li>
            <li class="has-sub active">
                <a href="javascript:;">
                    <b class="caret caret-right pull-right"></b>
                    <i class="ti-notepad"></i>
                    <span>Masterdata</span>
                </a>
                <ul class="sub-menu">
                    <li><a href="<?= site_url('news') ?>">News</a></li>
                    <li><a href="<?= site_url('gallery') ?>">Gallery</a></li>
                    <li class="active"><a href="<?= site_url('videos') ?>">Videos</a></li>
                </ul>
            </li>
            <li class="nav-divider"></li>
            <li class="nav-copyright">&copy; 2017 seanTheme All Right Reserved</li>
        </ul>
    </div>
    <a href="#" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="ti-arrow-left"></i></a>
</div>