<div id="content" class="content">
    <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><?= $url ?></a></li>
        <li class="breadcrumb-item active"><?= $sub_url ?></li>
    </ul>
    <h1 class="page-header">
        <?= $sub_url ?> <small>page header description goes here...</small>
    </h1>
    <div id="rootwizard" class="wizard wizard-full-width">
        <div class="wizard-header">
            <ul class="nav nav-pills">
                <li><a href="#tab1" data-toggle="tab">Add News</a></li>
                <li><a href="#tab2" data-toggle="tab" class="active">View News</a></li>
                <li><a href="#tab3" data-toggle="tab">Edit News</a></li>
            </ul>
        </div>
        <div class="wizard-content tab-content">
            <div class="tab-pane" id="tab1">
                <div class="row">
                    <div class="col-md-6 offset-md-3">
                        <p class="desc m-b-20">Make sure to use a valid email address, you'll need to verify it before you can login to your account.</p>
                        <form action="<?= site_url('news/add_news') ?>" method="POST" enctype="multipart/form-data" data-parsley-validate="true">
                            <div class="form-group">
                                <label class="control-label">Date <span class="text-danger">*</span></label>
                                <input type="date" class="form-control" value="<?= date('Y-m-d') ?>" name="date" />
                            </div>
                            <div class="form-group">
                                <label class="control-label">Title <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="title" placeholder="Input here.." data-parsley-required="true" />
                            </div>
                            <div class="form-group">
                                <label class="control-label">News <span class="text-danger">*</span></label>
                                <textarea class="form-control" name="news" rows="5" placeholder="Input here.." data-parsley-required="true"></textarea>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Writer <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="writer" readonly value="Mr. Sean Ngu" data-parsley-required="true" />
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" style="font-size: 13px;">Upload</span>
                                    </div>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01" name="file" data-parsley-required="true">
                                        <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary btn-sm m-r-3 m-b-3 start">
                                <i class="glyphicon glyphicon-upload"></i>
                                <span>Save</span>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="tab-pane active" id="tab2">
                <h1 class="page-header">
                    View News <small>page header description goes here...</small>
                </h1>
                <table id="datatables-default" class="table table-striped table-condensed table-bordered bg-white">
                    <thead>
                        <tr>
                            <th class="no-sort" style="width:1%">#</th>
                            <th style="white-space: nowrap">DATE</th>
                            <th style="white-space: nowrap">TITLE</th>
                            <th style="white-space: nowrap">NEWS</th>
                            <th style="white-space: nowrap">IMAGES</th>
                            <th style="white-space: nowrap">WRITER</th>
                            <th style="white-space: nowrap">STATUS</th>
                            <th class="no-sort" style="width:1%"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;
                        foreach ($data as $row) { ?>
                            <tr>
                                <td><?= $no; ?></td>
                                <td><?= date('d F Y', strtotime($row->date_upload)); ?></td>
                                <td><?= $row->title; ?></td>
                                <td><?= $row->news; ?></td>
                                <td align="center">
                                    <div style="width:50px;">
                                        <a href="<?= site_url('file/news/') . $row->img_upload; ?>" data-lightbox="1" data-title="<?= $row->img_upload; ?>">
                                            <img src="<?= site_url('file/news/') . $row->img_upload; ?>" alt="" class="img-fluid img-thumbnail">
                                        </a>
                                    </div>
                                </td>
                                <td><?= $row->writer; ?></td>
                                <td>
                                    <?php if ($row->status == '1') { ?>
                                        <label class="badge badge-success">
                                            <a style="color: #fff;" href="<?= site_url('news/non_active' . '/' . md5($row->news_id)); ?>" onclick="return confirm('are you sure non active data?')">Active</a>
                                        </label>
                                    <?php } elseif ($row->status == '0') { ?>
                                        <label class="badge badge-warning">
                                            <a style="color: #fff;" href="<?= site_url('news/active' . '/' . md5($row->news_id)); ?>" onclick="return confirm('are you sure active data?')">Not Active</a>
                                        </label>
                                    <?php } ?>
                                </td>
                                <td class="btn-col" style="white-space: nowrap">
                                    <a href="<?= site_url('news/edit_news' . '/' . md5($row->news_id)); ?>" class="btn btn-default btn-xs"><i class="ti-pencil"></i></a>
                                    <a href="<?= site_url('news/edit_news_img' . '/' . md5($row->news_id)); ?>" class="btn btn-default btn-xs"><i class="ti-image"></i></a>
                                </td>
                            </tr>
                        <?php $no++;
                        } ?>
                    </tbody>
                </table>
            </div>
            <div class="tab-pane" id="tab3">
                <div class="row">
                    <div class="col-md-6 offset-md-3">
                        <p class="desc m-b-20">Make sure to use a valid email address, you'll need to verify it before you can login to your account.</p>
                        <?php if ($update_news == "TRUE") { ?>
                            <form action="<?= site_url('news/update_news') ?>" method="POST" data-parsley-validate="true">
                                <input type="text" class="form-control" value="<?= md5($news_id) ?>" name="news_id" hidden />
                                <div class="form-group">
                                    <label class="control-label">Date <span class="text-danger">*</span></label>
                                    <input type="date" class="form-control" value="<?= $date; ?>" name="date" data-parsley-required="true" />
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Title <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="title" value="<?= $title_; ?>" data-parsley-required="true" />
                                </div>
                                <div class="form-group">
                                    <label class="control-label">News <span class="text-danger">*</span></label>
                                    <textarea class="form-control" name="news" rows="5" data-parsley-required="true"><?= $news; ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Writer <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="writer" value="<?= $writer; ?>" data-parsley-required="true" />
                                </div>
                                <button type="submit" class="btn btn-primary btn-sm m-r-3 m-b-3 start">
                                    <i class="glyphicon glyphicon-upload"></i>
                                    <span>Update</span>
                                </button>
                            </form>
                        <?php } ?>
                        <?php if ($update_news_img == "TRUE") { ?>
                            <form action="<?= site_url('news/update_news_img') ?>" method="POST" enctype="multipart/form-data" data-parsley-validate="true">
                                <input type="text" class="form-control" value="<?= md5($news_id) ?>" name="news_id" hidden />
                                <div style="width:150px;">
                                    <a href="<?= site_url('file/news/') . $images; ?>" data-lightbox="1" data-title="<?= $images; ?>">
                                        <img src="<?= site_url('file/news/') . $images; ?>" alt="" class="img-fluid img-thumbnail">
                                    </a>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" style="font-size: 13px;">Upload</span>
                                        </div>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01" name="file" data-parsley-required="true">
                                            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn-sm m-r-3 m-b-3 start">
                                    <i class="glyphicon glyphicon-upload"></i>
                                    <span>update</span>
                                </button>
                            </form>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>