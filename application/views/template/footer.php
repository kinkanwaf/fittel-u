<?php if ($this->session->flashdata('error_msg')) : ?>
    <a class="tst4"></a>
<?php endif; ?>

<?php if ($this->session->flashdata('succses')) : ?>
    <a class="tst1"></a>
<?php endif; ?>

<?php if ($this->session->flashdata('message')) : ?>
    <a class="tst2"></a>
<?php endif; ?>

<?php if ($this->session->flashdata('succses_msg')) : ?>
    <a class="tst3"></a>
<?php endif; ?>
<div id="footer" class="footer">
    &copy; 2017 seanTheme All Right Reserved
</div>
<a href="#" data-click="scroll-top" class="btn-scroll-top fade"><i class="ti-arrow-up"></i></a>
</div>
<script src="<?= site_url(); ?>assets/plugins/jquery/jquery-3.2.1.min.js"></script>
<script src="<?= site_url(); ?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?= site_url(); ?>assets/plugins/cookie/js/js.cookie.js"></script>
<script src="<?= site_url(); ?>assets/plugins/tooltip/popper/popper.min.js"></script>
<script src="<?= site_url(); ?>assets/plugins/bootstrap/bootstrap4/js/bootstrap.min.js"></script>
<script src="<?= site_url(); ?>assets/plugins/scrollbar/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?= site_url(); ?>assets/js/apps.min.js"></script>
<script src="<?= site_url(); ?>assets/plugins/loader/pace/pace.min.js"></script>
<script src="<?= site_url('') ?>assets/toast-master/js/jquery.toast.js"></script>
<script src="<?= site_url('') ?>assets/parsley/dist/parsley.js"></script>
<script src="<?= site_url(); ?>assets/plugins/form/bootstrap-wizard/js/bootstrap-wizard.min.js"></script>
<script src="<?= site_url(); ?>assets/js/page/form-wizards.demo.min.js"></script>
<script src="<?= site_url(); ?>assets/plugins/table/DataTables/JSZip-3.1.3/jszip.min.js"></script>
<script src="<?= site_url(); ?>assets/plugins/table/DataTables/pdfmake-0.1.27/build/pdfmake.min.js"></script>
<script src="<?= site_url(); ?>assets/plugins/table/DataTables/pdfmake-0.1.27/build/vfs_fonts.js"></script>
<script src="<?= site_url(); ?>assets/plugins/table/DataTables/DataTables-1.10.16/js/jquery.dataTables.min.js"></script>
<script src="<?= site_url(); ?>assets/plugins/table/DataTables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="<?= site_url(); ?>assets/plugins/table/DataTables/AutoFill-2.2.0/js/dataTables.autoFill.min.js"></script>
<script src="<?= site_url(); ?>assets/plugins/table/DataTables/AutoFill-2.2.0/js/autoFill.bootstrap.min.js"></script>
<script src="<?= site_url(); ?>assets/plugins/table/DataTables/Buttons-1.3.1/js/dataTables.buttons.min.js"></script>
<script src="<?= site_url(); ?>assets/plugins/table/DataTables/Buttons-1.3.1/js/buttons.bootstrap.min.js"></script>
<script src="<?= site_url(); ?>assets/plugins/table/DataTables/Buttons-1.3.1/js/buttons.colVis.min.js"></script>
<script src="<?= site_url(); ?>assets/plugins/table/DataTables/Buttons-1.3.1/js/buttons.flash.min.js"></script>
<script src="<?= site_url(); ?>assets/plugins/table/DataTables/Buttons-1.3.1/js/buttons.html5.min.js"></script>
<script src="<?= site_url(); ?>assets/plugins/table/DataTables/Buttons-1.3.1/js/buttons.print.min.js"></script>
<script src="<?= site_url(); ?>assets/plugins/table/DataTables/ColReorder-1.3.3/js/dataTables.colReorder.min.js"></script>
<script src="<?= site_url(); ?>assets/plugins/table/DataTables/FixedColumns-3.2.2/js/dataTables.fixedColumns.min.js"></script>
<script src="<?= site_url(); ?>assets/plugins/table/DataTables/FixedHeader-3.1.2/js/dataTables.fixedHeader.min.js"></script>
<script src="<?= site_url(); ?>assets/plugins/table/DataTables/KeyTable-2.2.1/js/dataTables.keyTable.min.js"></script>
<script src="<?= site_url(); ?>assets/plugins/table/DataTables/Responsive-2.1.1/js/dataTables.responsive.min.js"></script>
<script src="<?= site_url(); ?>assets/plugins/table/DataTables/Responsive-2.1.1/js/responsive.bootstrap.min.js"></script>
<script src="<?= site_url(); ?>assets/plugins/table/DataTables/RowGroup-1.0.0/js/dataTables.rowGroup.min.js"></script>
<script src="<?= site_url(); ?>assets/plugins/table/DataTables/RowReorder-1.2.0/js/dataTables.rowReorder.min.js"></script>
<script src="<?= site_url(); ?>assets/plugins/table/DataTables/Scroller-1.4.2/js/dataTables.scroller.min.js"></script>
<script src="<?= site_url(); ?>assets/plugins/table/DataTables/Select-1.2.2/js/dataTables.select.min.js"></script>
<script src="<?= site_url(); ?>assets/js/page/table-data.demo.min.js"></script>
<script>
    $(document).ready(function() {
        App.init();
        TableData.init();
    });
</script>
<script type="text/javascript">
    $(".tst4").show(function() {
        $.toast({
            heading: 'Error',
            text: '<?php echo $this->session->flashdata('error_msg'); ?>',
            position: 'bottom-right',
            loaderBg: 'rgba(255, 0, 0, 0.43)',
            icon: 'error',
            hideAfter: 6500,
            stack: 6
        });
    });

    $(".tst1").show(function() {
        $.toast({
            heading: 'Info',
            text: '<?php echo $this->session->flashdata('succses'); ?>',
            position: 'bottom-right',
            loaderBg: 'rgba(0, 151, 255, 0.43)',
            icon: 'info',
            hideAfter: 6500,
            stack: 6
        });
    });

    $(".tst2").show(function() {
        $.toast({
            heading: 'Warning',
            text: '<?php echo $this->session->flashdata('message'); ?>',
            position: 'bottom-right',
            loaderBg: 'rgba(255, 245, 0, 0.43)',
            icon: 'warning',
            hideAfter: 6500,
            stack: 6
        });
    });

    $(".tst3").show(function() {
        $.toast({
            heading: 'Succses',
            text: '<?php echo $this->session->flashdata('succses_msg'); ?>',
            position: 'bottom-right',
            loaderBg: 'rgba(53, 255, 0, 0.43)',
            icon: 'success',
            hideAfter: 5000
        });
    });
</script>
</body>

</html>