<?php
class m_data extends CI_Model
{
    public function add_news()
    {
        $data =
            [
                'news_id'       => ' ',
                'date_upload'   => $this->input->post('date'),
                'title'         => $this->input->post('title'),
                'news'          => $this->input->post('news'),
                'img_upload'    => 'default.png',
                'writer'        => $this->input->post('writer'),
                'status'        => '1'
            ];
        return $this->db->insert('news', $data);
    }

    public function check_file_upload()
    {
        $config['upload_path']          = './file/news';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 4000;
        $config['max_width']            = 4160;
        $config['max_height']           = 3120;
        $config['encrypt_name']         = TRUE;
        $config['remove_spaces']        = TRUE;
        $config['detect_mime']          = TRUE;
        $config['mod_mime_fix']         = TRUE;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('file')) {
            $return = ['result' => 'success', 'file' => $this->upload->data()];
            return $return;
        } else {
            $return = ['result' => 'failed'];
            return $return;
        }
    }

    public function save_file($upload)
    {
        $data = ['img_upload' => $upload['file_name']];
        $this->db->where('title', $this->input->post('title'));
        return $this->db->update('news', $data);
    }

    public function get_news()
    {
        return $this->db->get('news')->result();
    }

    public function get_news_id($id)
    {
        return $this->db->get_where('news', array('md5(news_id)' => $id))->row_array();
    }

    public function active_news($id)
    {
        $data = ['status' => 1];
        $this->db->where('news_id', $id);
        return $this->db->update('news', $data);
    }

    public function non_active_news($id)
    {
        $data = ['status' => 0];
        $this->db->where('news_id', $id);
        return $this->db->update('news', $data);
    }

    public function edit_news($id)
    {
        return $this->db->get_where('news', array('md5(news_id)' => $id, 'status' => '1'))->row_array();
    }

    public function update_news()
    {
        $data =
            [
                'date_upload'   => $this->input->post('date'),
                'title'         => $this->input->post('title'),
                'news'          => $this->input->post('news'),
                'writer'        => $this->input->post('writer'),
            ];
        $this->db->where('md5(news_id)', $this->input->post('news_id'));
        return $this->db->update('news', $data);
    }

    public function update_news_img($upload)
    {
        $data = ['img_upload' => $upload['file_name']];
        $this->db->where('md5(news_id)', $this->input->post('news_id'));
        return $this->db->update('news', $data);
    }

    // End of file model news

    public function add_videos()
    {
        $data =
            [
                'videos_id'  => ' ',
                'date'       => $this->input->post('date'),
                'title'      => $this->input->post('title'),
                'url'        => $this->input->post('url'),
                'status'     => '1'
            ];
        return $this->db->insert('videos', $data);
    }

    public function get_videos()
    {
        return $this->db->get('videos')->result();
    }

    public function get_videos_id($id)
    {
        return $this->db->get_where('videos', array('md5(videos_id)' => $id))->row_array();
    }

    public function active_videos($id)
    {
        $data = ['status' => 1];
        $this->db->where('videos_id', $id);
        return $this->db->update('videos', $data);
    }

    public function non_active_videos($id)
    {
        $data = ['status' => 0];
        $this->db->where('videos_id', $id);
        return $this->db->update('videos', $data);
    }

    public function edit_videos($id)
    {
        return $this->db->get_where('videos', array('md5(videos_id)' => $id, 'status' => '1'))->row_array();
    }

    public function update_videos()
    {
        $data =
            [
                'date'   => $this->input->post('date'),
                'title'  => $this->input->post('title'),
                'url'    => $this->input->post('url'),
            ];
        $this->db->where('md5(videos_id)', $this->input->post('videos_id'));
        return $this->db->update('videos', $data);
    }

    //End of file model videos

    public function add_gallery()
    {
        $data =
            [
                'gallery_id'    => ' ',
                'title'         => $this->input->post('title'),
                'img_upload'    => 'default.png',
                'status'        => '1'
            ];
        return $this->db->insert('gallery', $data);
    }

    public function check_file_upload_gallery()
    {
        $config['upload_path']          = './file/gallery';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 4000;
        $config['max_width']            = 4160;
        $config['max_height']           = 3120;
        $config['encrypt_name']         = TRUE;
        $config['remove_spaces']        = TRUE;
        $config['detect_mime']          = TRUE;
        $config['mod_mime_fix']         = TRUE;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('file')) {
            $return = ['result' => 'success', 'file' => $this->upload->data()];
            return $return;
        } else {
            $return = ['result' => 'failed'];
            return $return;
        }
    }

    public function save_file_gallery($upload)
    {
        $data = ['img_upload' => $upload['file_name']];
        $this->db->where('title', $this->input->post('title'));
        return $this->db->update('gallery', $data);
    }

    public function get_gallery()
    {
        return $this->db->get('gallery')->result();
    }
    public function get_gallery_id($id)
    {
        return $this->db->get_where('gallery', array('md5(gallery_id)' => $id))->row_array();
    }

    public function active_gallery($id)
    {
        $data = ['status' => 1];
        $this->db->where('gallery_id', $id);
        return $this->db->update('gallery', $data);
    }

    public function non_active_gallery($id)
    {
        $data = ['status' => 0];
        $this->db->where('gallery_id', $id);
        return $this->db->update('gallery', $data);
    }
}
