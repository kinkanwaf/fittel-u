-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 26, 2020 at 05:12 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `landing_page`
--

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `news_id` int(20) NOT NULL,
  `date_upload` date NOT NULL,
  `title` varchar(150) NOT NULL,
  `news` text NOT NULL,
  `img_upload` varchar(150) NOT NULL,
  `writer` varchar(150) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`news_id`, `date_upload`, `title`, `news`, `img_upload`, `writer`, `status`) VALUES
(1, '2020-01-26', 'Yummy', 'Copyright Disclaimer Under Section 107 of the Copyright Act 1976, allowance is made for \"fair use\" for purposes such as criticism, comment, news reporting, teaching, scholarship, and research. Fair use is a use permitted by copyright statute that might otherwise be infringing. Non-proft, educational or personal use tips the balance in favor of fair use.\r\nCategory\r\nMusic\r\nSuggested by UMG\r\nJustin Bieber - Yummy (Official Video)\r\nSong\r\nYummy\r\nArtist\r\nJustin Bieber\r\nLicensed to YouTube by\r\nUMG (on behalf of RBMG/Def Jam); Peermusic, UMPG Publishing, ASCAP, and 1 Music Rights Societies', '2f5f12a2446d6306d33100a344d0973e.png', 'Mr. Sean Ngua', 1),
(2, '2020-01-26', 'Ant Saunders - Yellow Hearts (Lyrics / Lyric Video)', 'Ant Saunders - Yellow Hearts (Lyrics / Lyric Video):\r\n\r\n[Chorus]\r\nShe put my name with yellow hearts\r\nHer favorite color, like the stars\r\nI didn\'t listen very?hard\r\nWhen?she told me?she was crazy from the start\r\nShe?put my name with yellow hearts (Yeah, she did)\r\nI said she was a work of art (Work of art)\r\nI didn\'t listen very hard (Very hard, no, no)\r\nWhen she told me she was crazy from the start\r\n\r\n[Verse 1]\r\nI drive down open roads so slow\r\nHere comes a train, I\'ll let it go\r\nAin\'t got nobody on my phone\r\nDon\'t like being all alone (Don\'t like being all alone)\r\nNot good at keeping with the trends (No, no)\r\nToo good at welcoming amends\r\nIt\'s been a while since I\'ve heard her say\r\nThat we were more than friends (Oh)\r\nOh, I, I-I-I, know a lot is going on\r\nBut girl, please tell me\r\nAnd I\'ll let you be\r\nAre you still with me or not?\r\n\r\n[Chorus]\r\nShe put my name with yellow hearts\r\nHer favorite color, like the stars\r\nI didn\'t listen very hard\r\nWhen she told me she was crazy from the start\r\nShe put my name with yellow hearts (Yeah, she did)\r\nI said she was a work of art (Work of art)\r\nI didn\'t listen very hard (Very hard, no, no)\r\nWhen she told me she was crazy from the start\r\n\r\n[Refrain]\r\nShe put yellow hearts around my name\r\nI thought they were all just the same\r\nTo you, what do they really mean?\r\nHave you only been playing games?\r\n\r\n[Verse 2]\r\nI\'m by the garden with the carpenter bees\r\nLike I\'m Bob Ross-in\' with a Harden beard\r\nTaking it all in like I got no issues in my nature\r\nPrimroses blooming in the night\r\nBirds wanna take me on a flight\r\nAppreciating my land \'fore it turns into a glacier\r\nChasers are of no good use\r\nThis taste will be long endured\r\nWhy\'d you have to go so soon?\r\nI thought this was as good as new\r\n\r\n[Interlude]\r\nShe put yellow hearts around my name\r\nI thought they were all just the same\r\nTo you, what do they really mean?\r\nHave you only been playing games?\r\n\r\n[Chorus]\r\nShe put my name with yellow hearts (Yeah, she did)\r\nHer favorite color, like the stars (Like the stars)\r\nI didn\'t listen very hard (Very hard, no, no)\r\nWhen she told me she was crazy from the start\r\nShe put my name with yellow hearts (She put yellow hearts around my name)\r\nI said she was a work of art (I thought they were all just the same)\r\nI didn\'t listen very hard (To you, what do they really mean?)\r\nWhen she told me she was crazy from the start (Have you only been playing games?)\r\n\r\n[Outro]\r\nHave you only been playing games?\r\nNo, no', 'b7f78cc9f14c5a6ab33ed4915782cd2a.jpg', 'Mr. Sean Ngua', 1);

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `videos_id` int(20) NOT NULL,
  `date` date NOT NULL,
  `title` varchar(225) NOT NULL,
  `url` varchar(150) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`videos_id`, `date`, `title`, `url`, `status`) VALUES
(1, '2020-01-26', 'Mahen - Pura Pura Lupa (Official Lyric Video)', 'https://www.youtube.com/embed/e8xGA0bLk2I', 1),
(2, '2020-01-26', 'Yummy - Justin Bieber \'Lirik Terjemahan Indonesia\' (Lyrics Video)', 'https://www.youtube.com/embed/8EJ3zbKTWQ8', 1),
(3, '2020-01-26', 'Hindi Rumah Ke Rumah ', 'https://www.youtube.com/embed/xTQvdE1oOaw?list=RDxTQvdE1oOaw', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`news_id`),
  ADD UNIQUE KEY `title` (`title`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`videos_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `news_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `videos_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
